# fspd Gentoo ebuild
# Created by 2019 Radim Kolar
# Public domain

EAPI=7

PYTHON_COMPAT=( python2_7 python3_6 )
inherit python-any-r1 multilib scons-utils toolchain-funcs

DESCRIPTION="FSP cli client"
HOMEPAGE="http://fspclient.sourceforge.net/"
SRC_URI="mirror://sourceforge/fspclient/fspclient/${PV}/${P}.tar.bz2"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

BDEPEND="sys-apps/sed"


src_configure() {
	MYSCONS=(
		CC="$(tc-getCC)"
		prefix=${D}
		mandir=${D}/usr/share/man
		docdir=${D}/usr/share/doc/${PF}
		)
}

src_compile() {
		escons "${MYSCONS[@]}"
}

src_install() {
		escons "${MYSCONS[@]}" install
}
